# Ludum Dare 38 - Island Attack

Made from scratch using:

- p5js
- es6

Tested on:

- Latest Chrome
- Latest Firefox

Play it here:

https://pflannery.gitlab.io/ld38-island-attack/

Jam entry here:

https://ldjam.com/events/ludum-dare/38/island-attack

Screenshots

![image](/screenshots/screen1.png)

![image](/screenshots/ld38-.gif)

![image](/screenshots/screen2.png)
