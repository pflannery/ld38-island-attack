/*
  Island Attack is a 2D game entry for Ludum Dare 38
  Copyright (C) 2017 Peter Flannery

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>
 */

let gameWorld

class PlayGameScene {

  constructor() {
    gameWorld = new World()
  }

  startGame() {
    gameWorld.init()
  }

  update(deltaTime) {
    const { kills, enemyCount } = PlayerManager.player

    if (kills === enemyCount) {
      SceneManager.loadScene("win")
      return
    }

    gameWorld.update(deltaTime)
  }

  draw() {
    gameWorld.draw()

    push()
    noStroke()
    translate(screenWidth - 150, 20)
    fill(255)
    text("CTRL+R to quit or restart", 0, 0)
    pop()
  }

}