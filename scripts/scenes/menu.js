/*
  Island Attack is a 2D game entry for Ludum Dare 38
  Copyright (C) 2017 Peter Flannery

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>
 */

class MenuScene {

  constructor() {
    this.flashRate = 200
    this.flashElasped = 0
    this.alpha = 1

    this.scrollDiff = 0.2
    this.scrollAmount = this.scrollDiff
  }


  update(deltaTime) {
    // enter
    if (keyIsDown(ENTER)) {
      SceneManager.loadScene("play")
      SceneManager.current.startGame()
      return
    }

    if (this.flashElasped > this.flashRate) {
      this.flashElasped = 0
      this.alpha = this.alpha === 1 ? 0 : 1
    }

    this.flashElasped += deltaTime

    // update the chunks
    ChunkManager.scrollX += this.scrollAmount
    if (ChunkManager.scrollX > screenWidth)
      this.scrollAmount = -this.scrollDiff
    else if (ChunkManager.scrollX < 0)
      this.scrollAmount = this.scrollDiff
  }

  draw() {

    // scroll the game as the background
    SceneManager.scenes["play"].draw()

    push()
    noStroke()

    translate(screenCentreX, screenCentreY)

    textSize(50)
    fill(255, 50, 100, (this.alpha * 100 + 150))
    text("Island Attack", -120, -40)

    textSize(24)
    fill(255)
    text("Start a new game", -80, 0)

    textSize(14)
    fill(255, 255, 255, this.alpha * 255)
    text("Press ENTER to begin", -55, 30)

    textSize(14)
    fill(100, 255, 25)
    text("W and S to turn", -36, 60)
    text("A and D to boost speed", -58, 80)
    text("SPACE to fire guns", -44, 100)
    text("E to fire bombs", -30, 120)
    pop()
  }

}
