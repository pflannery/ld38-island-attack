/*
  Island Attack is a 2D game entry for Ludum Dare 38
  Copyright (C) 2017 Peter Flannery

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>
 */

class WinScene {

  constructor() {
    this.fadeLifetime = 255

    this.scrollDiff = 0.2
    this.scrollAmount = this.scrollDiff
  }

  update(deltaTime) {

    // enter
    if (keyIsDown(ENTER)) {
      SceneManager.loadScene("play")
      SceneManager.current.startGame()
      return
    }

    this.fadeLifetime -= deltaTime

    if (this.fadeLifetime <= 0) {
      this.fadeLifetime = 255
    }

    // update the chunks
    ChunkManager.scrollX += this.scrollAmount
    if (ChunkManager.scrollX > screenWidth)
      this.scrollAmount = -this.scrollDiff
    else if (ChunkManager.scrollX < 0)
      this.scrollAmount = this.scrollDiff
  }

  draw() {
    blendMode(DODGE)

    // scroll the game as the background
    SceneManager.scenes["play"].draw()

    blendMode(BLEND)
    // write the game over text
    push()
    noStroke()
    fill(100, 255, 100, this.fadeLifetime)
    textSize(24)
    translate(screenCentreX, screenCentreY)
    text("You Win!!!!!!!!!!", -50, 0)
    fill(255)
    textSize(14)
    text("Press ENTER to play again", -65, 16)
    pop()

  }

}