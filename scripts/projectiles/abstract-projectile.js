/*
  Island Attack is a 2D game entry for Ludum Dare 38
  Copyright (C) 2017 Peter Flannery

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>
 */

class AbstractProjectile {

  constructor(pos, dir, owner, hitTargetCb, bulletSpeed, damage = 3, size = 2) {
    this.position = pos.copy()
    this.direction = dir.copy()
    this.damage = damage
    this.speed = bulletSpeed
    this.lifetime = 2500
    this.size = size
    this.owner = owner
    this.hitTargetCallback = hitTargetCb

    this.collider = new CircleCollider(
      this.size, this, null
    )
  }

  get alive() {
    return this.lifetime > 0
  }

  notifyHit() {
    if (this.hitTargetCallback)
      this.hitTargetCallback.call(this.owner, this)
  }

  update(deltaTime) {
    let velocity = createVector(
      this.direction.x * this.speed * deltaTime,
      this.direction.y * this.speed * deltaTime
    )

    this.position.add(velocity)
    this.lifetime -= deltaTime
    this.updateCollider()
  }

  updateCollider() {
    const hs = this.size * .5

    this.collider.set(
      createVector(
        this.position.x - hs,
        this.position.y - hs
      )
    )

    if (this.alive === false)
      this.collider.destroy()
  }

  destroy() {
    this.lifetime = 0
    this.collider.destroy()
  }

  draw() {
    const hs = this.size * .5
    noStroke()
    fill(255)
    const { x, y } = this.position
    ellipse(x - hs, y - hs, this.size)
  }

}