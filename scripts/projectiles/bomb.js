/*
  Island Attack is a 2D game entry for Ludum Dare 38
  Copyright (C) 2017 Peter Flannery

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>
 */

class Bomb extends AbstractProjectile {

  constructor(pos, dir, owner, hitTargetCb, damage = 10) {
    super(
      pos,
      dir,
      owner,
      hitTargetCb,
      // bullet speed
       0.10,
      damage,
      // radius size
      8
    )

    this.collider.r = this.size * 3
  }

  update(deltaTime) {
    this.position.y += this.speed * deltaTime
    this.lifetime -= deltaTime
    this.updateCollider()
    this.speed += 0.005
  }

  draw() {
    const hs = this.size * .5
    noFill()
    stroke(255)
    const { x, y } = this.position
    ellipse(x - hs, y - hs, this.size * .7, this.size)
    // this.collider.debugDraw()
  }

}