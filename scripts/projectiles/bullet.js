/*
  Island Attack is a 2D game entry for Ludum Dare 38
  Copyright (C) 2017 Peter Flannery

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>
 */

class Bullet extends AbstractProjectile {

  constructor(pos, dir, owner, hitTargetCb, bulletSpeed, damage = 3, size = 1) {
    super(
      pos,
      dir,
      owner,
      hitTargetCb,
      bulletSpeed,
      damage,
      size
    )
    this.collider.r = 1
  }

  update(deltaTime) {
    let velocity = createVector(
      this.direction.x * this.speed * deltaTime,
      this.direction.y * this.speed * deltaTime
    )

    this.position.add(velocity)
    this.lifetime -= deltaTime
    this.updateCollider()
  }

  draw() {
    noStroke()
    fill(255)
    const { x, y } = this.position
    ellipse(x - 1, y - 1, this.size)
    //  this.collider.debugDraw()
  }

}

class AirplaneBullet extends Bullet {

  constructor(pos, dir, owner, hitTargetCb, damage = 3) {
    super(
      pos,
      dir,
      owner,
      hitTargetCb,
      // bullet speed 
      random(0.6, 0.65),
      damage
    )

  }

}

class SoldierBullet extends Bullet {

  constructor(pos, dir, owner, hitTargetCb, damage = 3) {
    super(
      pos,
      dir,
      owner,
      hitTargetCb,
      // bullet speed 
      random(0.2, 0.25),
      damage
    )
  }

}

class TurretBullet extends Bullet {

  constructor(pos, dir, owner, hitTargetCb, damage = 3) {
    super(
      pos,
      dir,
      owner,
      hitTargetCb,
      // bullet speed 
      random(0.3, 0.32),
      damage,
      3
    )
  }

}