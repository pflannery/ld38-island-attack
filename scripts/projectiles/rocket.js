/*
  Island Attack is a 2D game entry for Ludum Dare 38
  Copyright (C) 2017 Peter Flannery

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>
 */

class Rocket extends AbstractProjectile {

  constructor(pos, dir, owner, hitTargetCb, damage = 3) {
    super(
      pos,
      dir,
      owner,
      hitTargetCb,
      // bullet speed 
      random(0.2, 0.213),
      damage
    )
    this.seeker = true
  }

  update(deltaTime) {
    let velocity = createVector(
      this.direction.x * this.speed * deltaTime,
      this.direction.y * this.speed * deltaTime
    )

    this.position.add(velocity)
    this.lifetime -= deltaTime
    this.updateCollider()
  }

  draw() {

    push()
    strokeWeight(2)
    noFill(255)
    const { x, y } = this.position
    stroke(255, 200, 100)
    line(x, y, x + (this.direction.x * 6), y + (this.direction.y * 6))
    // ellipse(x - 1, y - 1, this.size)
    // this.collider.debugDraw()
    pop()
  }

}