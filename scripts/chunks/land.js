/*
  Island Attack is a 2D game entry for Ludum Dare 38
  Copyright (C) 2017 Peter Flannery

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>
 */

class LandChunk extends AbstractChunk {

  constructor(index, x, width) {
    super(index, x, width, 20)
    this.color = random(120, 200)

    this.treeCount = random(12, 20) | 0
    this.trees = []
    this.generateTrees()

    this.turretCount = random(3, 8) | 0
    this.turrets = []
    this.generateTurrets()

    this.soldierCount = random(20, 30) | 0
    this.soldiers = []
    this.generateSoldiers()

    PlayerManager.player.enemyCount += this.turretCount
    PlayerManager.player.enemyCount += this.soldierCount
  }

  generateTrees() {
    const cellSize = this.width / this.treeCount
    const cellSpacing = cellSize * .1 // 10% of the cell size
    for (let i = 0; i < this.treeCount; i++) {
      let minCellX = i * cellSize
      let maxCellX = minCellX + cellSize
      minCellX += cellSpacing
      maxCellX -= cellSpacing
      let sx = this.startX + random(minCellX, maxCellX)
      this.trees.push(
        new Tree(
          sx,
          screenHeight - 20
        )
      )
    }
  }

  generateTurrets() {
    const cellSize = this.width / this.turretCount
    const cellSpacing = cellSize * .2 // 20% of the cell size
    for (let i = 0; i < this.turretCount; i++) {
      let minCellX = i * cellSize
      let maxCellX = minCellX + cellSize
      minCellX += cellSpacing
      maxCellX -= cellSpacing
      let sx = this.startX + random(minCellX, maxCellX)
      this.turrets.push(
        new Turret(
          sx,
          screenHeight - 20
        )
      )
    }
  }

  generateSoldiers() {
    const cellSize = this.width / this.soldierCount
    const cellSpacing = cellSize * .2 // 20% of the cell size
    for (let i = 0; i < this.soldierCount; i++) {
      let minCellX = i * cellSize
      let maxCellX = minCellX + cellSize
      minCellX += cellSpacing
      maxCellX -= cellSpacing
      let sx = this.startX + random(minCellX, maxCellX)
      this.soldiers.push(
        new Soldier(
          this.startX, this.endX,
          screenHeight - 20
        )
      )
    }
  }

  onMove(diff) {
    this.trees.forEach(tree => {
      // tree.onMove(diff)
      tree.position.x += diff
    })
    this.turrets.forEach(
      turret => turret.onMove(diff)
    )
    this.soldiers.forEach(
      soldier => soldier.onMove(diff)
    )
  }

  update(deltaTime) {
    this.updateTurrets(deltaTime)
    this.updateSoldiers(deltaTime)
    super.update(deltaTime)
  }

  updateTurrets(deltaTime) {
    this.turrets.forEach(
      turret => turret.update(deltaTime)
    )
  }

  updateSoldiers(deltaTime) {
    this.soldiers.forEach(
      soldier => soldier.update(deltaTime)
    )
  }

  draw() {
    noStroke()
    fill(0, this.color, 70)
    rect(this.startX, screenHeight - 20, this.width, 20)

    this.drawTrees()
    this.drawTurrets()
    this.drawSoldiers()
  }

  drawTurrets() {
    this.turrets.forEach(
      turret => turret.draw()
    )
  }

  drawSoldiers() {
    this.soldiers.forEach(
      soldier => soldier.draw()
    )
  }

  drawTrees() {
    this.trees.forEach(
      tree => tree.draw()
    )
  }

}
