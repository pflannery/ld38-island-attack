/*
  Island Attack is a 2D game entry for Ludum Dare 38
  Copyright (C) 2017 Peter Flannery

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>
 */

class AbstractChunk {

  constructor(index, x, width, height) {
    this.startX = x
    this.endX = x + width
    this.width = width
    this.height = height
    this.id = index

    this.collider = new RectCollider(
      this.startX,
      screenHeight - this.height,
      this.width,
      this.height,
      this,
      this.onColliderNotify
    )
    this.updateCollider();
  }

  moveStart(x) {
    const diff = x - this.startX
    this.startX = x
    this.endX = x + this.width

    if (this.onMove)
      this.onMove(diff)
  }

  moveEnd(x) {
    const diff = x - this.endX
    this.startX = x - this.width
    this.endX = x

    if (this.onMove)
      this.onMove(diff)
  }

  within(x1, x2) {
    return (this.startX >= x1 && this.startX <= x2)
      || (this.endX > x1 && this.endX <= x2)
  }

  update() {
    this.updateCollider()
  }

  debugDraw() {
    text(this.id, this.startX + 100, screenCentreY)
  }

  onColliderNotify(collisionInfo) {
    if (collisionInfo.owner instanceof Airplane) {
      const plane = collisionInfo.owner
      plane.health = max(plane.health - this.damage, 0)

      if (!plane.alive) {
        plane.speed = 0
        plane.collider.destroy()
        ExplosionManager.add(
          new AirplaneExplodeParticle(plane.position),
          plane.onExplosionFinished
        )
      }

    } else if (collisionInfo.owner instanceof Bullet) {

      const bullet = collisionInfo.owner

      ExplosionManager.add(
        new BulletExplodeParticle(bullet.position),
        ExplosionManager.destroy
      )

      bullet.notifyHit()
    } else if (collisionInfo.owner instanceof Bomb) {

      const bullet = collisionInfo.owner

      ExplosionManager.add(
        new BombExplodeParticle(bullet.position),
        ExplosionManager.destroy
      )

      bullet.notifyHit()
    }

  }

  updateCollider() {
    this.collider.set(
      createVector(this.startX, screenHeight - this.height)
    )
  }


}

