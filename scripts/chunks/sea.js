/*
  Island Attack is a 2D game entry for Ludum Dare 38
  Copyright (C) 2017 Peter Flannery

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>
 */

class SeaChunk extends AbstractChunk {

  constructor(index, x, width) {
    super(index, x, width, 10)
    this.triangleWidth = 20
    this.triangleCount = (width / this.triangleWidth) | 0
    this.damage = 1000

    this.boatCount = 2
    this.boats = []
    this.generateBoats()
  }

  onMove(diff) {
    this.boats.forEach(
      boat => {
        boat.position.x += diff
        boat.onMove(diff)
      }
    )
  }

  generateBoats() {
    const cellSize = this.width / this.boatCount
    const cellSpacing = cellSize * .3 // 30% of the cell size
    for (let i = 0; i < this.boatCount; i++) {
      let minCellX = i * cellSize
      let maxCellX = minCellX + cellSize
      minCellX += cellSpacing
      maxCellX -= cellSpacing
      let sx = this.startX + random(minCellX, maxCellX)
      this.boats.push(
        new Boat(
          sx,
          screenHeight - this.height
        )
      )
    }

  }

  update(deltaTime) {
    this.updateBoats(deltaTime)
    super.update(deltaTime)
  }

  updateBoats(deltaTime) {
    this.boats.forEach(
      boat => {
        boat.update(deltaTime)
      }
    )
  }

  draw() {
    this.drawBoats()
    this.drawWater()
  }

  drawBoats() {
    this.boats.forEach(
      boat => {
        boat.draw()
      }
    )
  }

  drawWater() {
    // background water
    this.drawTriangleStrip(
      0, 0,
      0, 70, 150, 255
    )
    // foreground water
    this.drawTriangleStrip(
      10, 0,
      0, 100, 200, 200
    )
  }

  drawTriangleStrip(xoffset, yoffset, r, g, b, a) {
    stroke(255, 255, 255, 80)
    fill(r, g, b, a)

    let x1 = this.startX + xoffset
    let x2 = x1 + (this.triangleWidth * .5)
    let x3 = x1 + this.triangleWidth

    let y1 = screenHeight - yoffset
    let y2 = y1 - this.height
    let y3 = y1

    // background water
    for (var i = 0; i < this.triangleCount; i++) {
      triangle(
        x1, y1,
        x2, y2,
        x3, y3
      )

      x1 += this.triangleWidth
      x2 += this.triangleWidth
      x3 += this.triangleWidth
    }

  }

}