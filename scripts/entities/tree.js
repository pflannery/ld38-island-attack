/*
  Island Attack is a 2D game entry for Ludum Dare 38
  Copyright (C) 2017 Peter Flannery

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>
 */

class Tree {

  constructor(x, y) {
    this.position = createVector(x, y)
    this.height = random(12, 20)
  }

  onMove(diff) {
    this.position.x += diff
  }

  draw() {
    const { x, y } = this.position
    this.drawTrunk(x, y)
    this.drawLeaves(x, y)
  }

  drawTrunk(x, y) {
    const hw = 2
    fill(200, 50, 0, 100)
    noStroke()
    rect(
      x - hw,
      y - this.height,
      hw * 2,
      this.height
    )
  }

  drawLeaves(x, y) {
    fill(0, 200, 70, 200)
    stroke(0, 100, 0, 160)

    // left
    ellipse(
      x - 4,
      y - this.height,
      20
    )

    // right
    ellipse(
      x + 4,
      y - this.height,
      20
    )

    // top
    fill(0, 200, 70, 230)
    ellipse(
      x,
      y - this.height - 4,
      20
    )

  }


}