/*
  Island Attack is a 2D game entry for Ludum Dare 38
  Copyright (C) 2017 Peter Flannery

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>
 */

class Turret {

  constructor(x, y) {
    this.width = 20
    this.position = createVector(x, y)
    this.height = 4
    this.canonHeight = 4

    this.startingHealth = 5
    this.health = this.startingHealth

    this.collider = new CircleCollider(
      this.width,
      this,
      this.onColliderNotify
    )

    this.gunPos = createVector(0, 0)
    this.gun = new Gun(
      // fireInterval
      1000,
      // fire radius
      screenWidth / 2,
      // owner
      this
    )
    this.gun.setAmmoType(TurretBullet)
  }

  get alive() {
    return this.health > 0
  }

  onMove(diff) {
    this.position.x += diff
    if (this.smokeEngine)
      this.smokeEngine.emitPosition.x += diff;
  }

  update(deltaTime) {
    this.updateGun(deltaTime)
    this.updateCollider()
  }

  updateGun(deltaTime) {
    this.gun.update(deltaTime)
    if (!this.alive)
      return

    this.gunPos = createVector(
      this.position.x + (this.width * .5),
      this.position.y - this.height
    )

    this.gun.fireWithinRange(
      this.gunPos,
      PlayerManager.player.position
    )
  }

  updateCollider() {
    this.collider.set(
      Vector.add(
        this.position,
        createVector(this.width * .5, 0)
      )
    )
  }

  onColliderNotify(collisionInfo) {
    if (collisionInfo.owner instanceof Bullet
      || collisionInfo.owner instanceof Bomb) {

      const bullet = collisionInfo.owner

      // make sure this turrent cant shoot itself
      if (bullet.owner === this.gun)
        return

      // this will stop not friendly fire
      const gun = bullet.owner
      if (gun.owner instanceof Turret ||
        gun.owner instanceof Soldier)
        return

      this.health = max(this.health - bullet.damage, 0)

      // check if this turrent is dead
      if (!this.alive) {
        gun.owner.kills += 1
        this.collider.destroy()

        ExplosionManager.add(
          new TurretExplodeParticle(this.position),
          ExplosionManager.destroy
        )

        this.smokeEngine = new TurretSmokeEngine(
          this.gunPos,
          ParticleEngineManager.destroy
        )

      }

      bullet.notifyHit()
    }

  }

  draw() {
    const { x, y } = this.position

    if (this.alive)
      this.drawTurret(x, y)

    this.drawHealth(x, y)
  }

  drawHealth(x, y) {
    push()
    fill(100, 100, 100)
    noStroke()
    translate(x, y - this.height)

    // health taken in red
    stroke(255)
    fill(255, 0, 0)
    rect(
      0,
      0,
      this.width,
      this.height
    )

    // bunker health left over
    noStroke()
    fill(0, 255, 0)
    rect(
      0,
      1,
      (this.health / this.startingHealth) * this.width,
      this.height - 1
    )
    pop()
  }

  drawTurret(x, y) {
    fill(25, 25, 25)
    const lineWidth = 6

    push()
    translate(this.gunPos.x, this.gunPos.y)
    stroke(255, 255, 255, 100)
    strokeWeight(4)
    line(
      0, 0,
      lineWidth * this.gun.forward.x, lineWidth * this.gun.forward.y
    )
    pop()

    this.gun.draw()
  }

}