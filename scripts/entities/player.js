/*
  Island Attack is a 2D game entry for Ludum Dare 38
  Copyright (C) 2017 Peter Flannery

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>
 */

class Player extends Airplane {

  constructor(x, y) {
    super(x, y)
    this.startingHealth = 100
    this.health = this.startingHealth
    this.enemyCount = 0
    this.speed = .2

    this.startingBoostCharge = 100
    this.boostCharge = this.startingBoostCharge
    this.dischargeRate = 1.8
    this.rechargeRate = 0.3

    this.boostSpeed = .3
    this.normalSpeed = this.speed
  }

  update(deltaTime) {
    // space (fire)
    if (keyIsDown(32))
      this.guns[0].fire(this.position, this.forward)

    // A or D (speed boost)
    if (
      (keyIsDown(65) && this.forward.x < 0) ||
      (keyIsDown(68) && this.forward.x >= 0)) {
      this.boostCharge = max(
        this.boostCharge - this.dischargeRate,
        0
      )

      if (this.boostCharge > 0)
        this.speed = min(this.speed + 0.01, this.boostSpeed)
      else
        this.speed = max(this.speed - 0.01, this.normalSpeed)

    } else {
      this.boostCharge = min(
        this.boostCharge + this.rechargeRate,
        this.startingBoostCharge
      )

      this.speed = max(this.speed - 0.01, this.normalSpeed)
    }


    // W
    if (keyIsDown(87))
      this.rotation -= this.turnSpeed * deltaTime

    // S
    else if (keyIsDown(83))
      this.rotation += this.turnSpeed * deltaTime

    // E
    else if (keyIsDown(69))
      this.guns[1].fire(this.position, this.forward)


    // ensure player stay in screen
    this.position.y = max(this.position.y, 0)


    super.update(deltaTime)
  }

  onExplosionFinished(instance) {
    SceneManager.loadScene("gameover")
  }

}