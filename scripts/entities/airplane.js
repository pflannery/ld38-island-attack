/*
  Island Attack is a 2D game entry for Ludum Dare 38
  Copyright (C) 2017 Peter Flannery

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>
 */

const AirplaneStartingHealth = 5

class Airplane {

  constructor(x, y) {
    this.height = 8
    this.width = 32
    this.speed = 0.2
    this.turnSpeed = 0.2
    this.startingHealth = AirplaneStartingHealth
    this.health = AirplaneStartingHealth
    this.kills = 0
    this.position = createVector(
      x,
      y
    )

    this.guns = []

    this.acceleration = createVector(0, 0)
    this.forward = createVector(-1, 0)
    this.up = createVector(0, -1)
    this.rotation = 0
    this.collider = new CircleCollider(
      this.width, this, this.onColliderNotify
    )
    this.flipped = false

    this.updateCollider()
    this.generateGuns()
  }

  get alive() {
    return this.health > 0
  }

  generateGuns() {
    const machineGun = new Gun(
      // fireInterval
      100,
      // fire radius
      100,
      // owner
      this
    )
    machineGun.setAmmoType(AirplaneBullet)
    this.guns.push(machineGun)

    const bombLauncher = new Gun(
      // fireInterval
      500,
      // fire radius
      100,
      // owner
      this
    )

    bombLauncher.setAmmoType(Bomb)
    this.guns.push(bombLauncher)
  }

  onColliderNotify(collisionInfo) {
    if (collisionInfo.owner instanceof AbstractProjectile) {
      const bullet = collisionInfo.owner
      const gun = bullet.owner

      // make sure we dont count our own bullets
      if (this.guns.indexOf(gun) > -1)
        return

      this.health = max(this.health - bullet.damage, 0)
      if (!this.alive) {
        this.planeCrashSmoke = new PlaneSmokeEngine(
          this.position,
          this.forward
        )
      }

      // explode the bullet
      ExplosionManager.add(
        new BulletExplodeParticle(bullet.position),
        ExplosionManager.destroy
      )

      bullet.notifyHit()
    } else if (collisionInfo.owner instanceof AbstractChunk) {

      this.collider.destroy()
      this.collider = null
      // explode the airplane
      ExplosionManager.add(
        new AirplaneExplodeParticle(this.position),
        this.onExplosionFinished
      )

    }

  }

  onExplosionFinished(instance) {
    ExplosionManager.destroy(instance)
  }

  update(deltaTime) {
    const velocity = this.speed * deltaTime

    const testFlipRot = this.rotation % 360
    this.flipped = testFlipRot < -90 && testFlipRot > -270

    const fwdRotation = radians(this.rotation)
    const upRotation = radians(this.rotation - 90)

    if (!this.planeCrashSmoke) {
      this.forward.x = cos(fwdRotation)
      this.forward.y = sin(fwdRotation)

      this.up.x = cos(upRotation)
      this.up.y = sin(upRotation)
    } else {
      this.forward.y = min(this.forward.y + 0.2, 0)
    }

    // if (upRotation > PI / 2.1 && upRotation < PI / 1.9) {
    let acceleration = Vector.mult(this.forward, velocity)
    this.position.add(acceleration)

    if (this.planeCrashSmoke)
      this.position.y += 1.7

    if (this.planeCrashSmoke)
      this.planeCrashSmoke.emitPosition = this.position;

    this.updateGuns(deltaTime)

    if (this.collider)
      this.updateCollider()
  }

  updateGuns(deltaTime) {
    this.guns.forEach(
      gun => {
        gun.forward = this.forward
        gun.update(deltaTime)
      }
    )
  }

  updateCollider() {
    this.collider.set(this.position)
  }

  draw() {
    if (!this.alive && !this.collider)
      return

    noStroke()
    fill(160, 160, 160)
    push()

    translate(
      this.position.x,
      this.position.y
    )

    rotate(radians(this.rotation))

    this.drawBody()
    this.drawTail()
    this.drawCockpit()
    this.drawNose()
    this.drawWing()

    pop()

    this.drawGuns()
    // this.debugDraw()
  }

  drawGuns() {
    this.guns.forEach(
      gun => gun.draw()
    )
  }

  drawBody() {
    noStroke()
    rect(
      -this.width * .5,
      -this.height * .5,
      this.width,
      this.height
    )
  }

  drawTail() {
    noStroke()

    const hw = this.width * .5
    const hh = this.height * .5

    let x1 = -hw
    let x2 = x1
    let x3 = x1 + 10

    let y1 = -hh + 1
    let y2 = y1 - 6
    let y3 = y1

    if (this.flipped) {
      y1 = -y1
      y2 = -y2
      y3 = -y3
    }

    triangle(
      x1, y1,
      x2, y2,
      x3, y3
    )
  }

  drawCockpit() {
    noStroke()
    let x1 = 5
    let y1 = -this.height * .5 + 2

    if (this.flipped) {
      y1 = -y1
    }

    ellipse(x1, y1, 10, 7)
  }

  drawNose() {
    noStroke()

    let x1 = this.width * .5 - 1
    let x2 = x1 + 12
    let x3 = x1

    let y1 = -this.height * .5
    let y2 = this.height * .5
    let y3 = this.height * .5

    if (this.flipped) {
      y1 = -y1
      y2 = -y2
      y3 = -y3
    }

    triangle(
      x1, y1,
      x2, y2,
      x3, y3
    )

  }

  drawWing() {
    let y = -1
    if (this.flipped) {
      y = -y - 3
    }

    stroke(0, 0, 0, 70)
    rect(
      -5,
      y,
      16,
      2
    )
  }

  debugDraw() {
    push()
    let lineWidth = 100
    translate(this.position.x, this.position.y)
    stroke(255)
    line(0, 0, lineWidth * this.forward.x, lineWidth * this.forward.y)
    line(0, 0, lineWidth * this.up.x, lineWidth * this.up.y)

    pop()

    if (this.collider)
      this.collider.debugDraw()
  }

}