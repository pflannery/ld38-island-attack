/*
  Island Attack is a 2D game entry for Ludum Dare 38
  Copyright (C) 2017 Peter Flannery

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>
 */

class Gun {

  constructor(fireInterval, fireRadius, owner) {
    this.owner = owner
    this.projectiles = []
    this.fireInterval = fireInterval
    this.lastFireTime = -1
    this.fireRadius = fireRadius
    this.forward = createVector(0, -1)
  }

  setAmmoType(projectileClass) {
    this.ProjectileClass = projectileClass
  }

  isTargetInRange(gunPos, target) {
    return (pointInCircle(
      target.x, target.y,
      gunPos.x, gunPos.y,
      this.fireRadius
    ))
  }

  fireWithinRange(gunPos, target) {
    const delta = Vector.sub(target, gunPos)
    delta.normalize()
    this.forward = delta

    // if the player is in range then fire
    if (pointInCircle(
      target.x, target.y,
      gunPos.x, gunPos.y,
      this.fireRadius
    )) {
      return this.fire(
        gunPos,
        this.forward
      )
    }

    return false
  }

  fire(target) {
    if (this.lastFireTime > 0)
      return false

    this.lastFireTime = this.fireInterval
    this.projectiles.push(
      new this.ProjectileClass(
        target,
        this.forward,
        this,
        this.onBulletHitTarget
      )
    )

    return true
  }

  onBulletHitTarget(bullet) {
    const i = this.projectiles.indexOf(bullet)
    this.projectiles.splice(i, 1)
    bullet.destroy()
  }

  update(deltaTime, target) {
    for (let i = this.projectiles.length - 1; i >= 0; i--) {
      const projectile = this.projectiles[i]
      if (projectile.alive === false) {
        this.projectiles.splice(i, 1)
        continue
      }

      projectile.update(deltaTime)
      if (projectile.seeker) {
        const delta = Vector.sub(target, projectile.position)
        delta.normalize()
        projectile.direction = delta.copy()
      }
    }

    this.lastFireTime -= deltaTime
  }

  draw() {
    this.projectiles.forEach(
      bullet => {
        bullet.draw()
      }
    )
  }


}