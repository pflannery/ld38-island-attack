/*
  Island Attack is a 2D game entry for Ludum Dare 38
  Copyright (C) 2017 Peter Flannery

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>
 */

class Boat {

  constructor(x, y) {
    this.width = 100
    this.height = 10
    this.position = createVector(x, y - this.height + 4)

    this.turretCount = random(1, 3) | 0
    this.turrets = []
    this.generateTurrets()

    this.soldierCount = random(3, 5) | 0
    this.soldiers = []
    this.generateSoldiers()

    PlayerManager.player.enemyCount += this.turretCount
    PlayerManager.player.enemyCount += this.soldierCount
  }

  generateTurrets() {
    const offsetX = this.position.x
    const offsetY = this.position.y

    const cellSize = this.width / this.turretCount
    const cellSpacing = 12
    for (let i = 0; i < this.turretCount; i++) {
      let minCellX = i * cellSize
      let maxCellX = minCellX + cellSize
      minCellX += cellSpacing
      maxCellX -= cellSpacing
      let sx = offsetX + random(minCellX, maxCellX)
      this.turrets.push(
        new Turret(
          sx,
          offsetY
        )
      )
    }
  }

  generateSoldiers() {
    const offsetX = this.position.x
    const offsetY = this.position.y

    const cellSize = this.width / this.soldierCount
    const cellSpacing = 12
    for (let i = 0; i < this.soldierCount; i++) {
      let minCellX = i * cellSize
      let maxCellX = minCellX + cellSize
      minCellX += cellSpacing
      maxCellX -= cellSpacing
      this.soldiers.push(
        new Soldier(
          offsetX + minCellX, offsetX + maxCellX,
          offsetY
        )
      )
    }
  }


  onMove(diff) {
    this.turrets.forEach(
      turret => turret.onMove(diff)
    )
    this.soldiers.forEach(
      soldier => soldier.onMove(diff)
    )
  }

  update(deltaTime) {
    this.updateTurrets(deltaTime)
    this.updateSoldiers(deltaTime)
  }

  updateSoldiers(deltaTime) {
    this.soldiers.forEach(
      soldier => soldier.update(deltaTime)
    )
  }

  updateTurrets(deltaTime) {
    this.turrets.forEach(
      turret => {
        turret.update(deltaTime)
      }
    )
  }

  draw() {
    const { x, y } = this.position
    const hh = this.height * .5
    const qw = this.width * .25

    push()
    noStroke()
    fill(255, 255, 255, 50)
    rect(x, y, this.width, this.height)
    rect(x + qw, y - hh, this.width - qw - qw, hh)
    pop()

    this.drawTurrets()
    this.drawSoldiers()

  }

  drawTurrets() {
    this.turrets.forEach(
      turret => {
        turret.draw()
      }
    )
  }

  drawSoldiers() {
    this.soldiers.forEach(
      soldier => soldier.draw()
    )
  }

}