/*
  Island Attack is a 2D game entry for Ludum Dare 38
  Copyright (C) 2017 Peter Flannery

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>
 */

class Soldier {

  constructor(minX, maxX, y) {
    this.minX = minX
    this.maxX = maxX
    this.width = 3
    this.height = 8
    this.speed = 0.01
    this.startingHealth = 3
    this.health = this.startingHealth

    //
    this.position = createVector(
      random(minX, maxX),
      y
    )
    this.moveTarget = createVector(
      random(minX, maxX),
      y
    )

    //
    this.collider = new CircleCollider(
      6,
      this,
      this.onColliderNotify
    )

    // gun stuff
    this.gunPos = createVector(0, 0)

    // generate random and covert to int. 
    // 1 in 4 chance of being a rocket
    const spawnRocket = (random(4) | 0) === 1
    const fireInterval = spawnRocket ? 2000 : 1000
    const ammoType = spawnRocket ? Rocket : SoldierBullet

    this.gun = new Gun(
      // fireInterval
      fireInterval,
      // fire radius
      spawnRocket ? screenHeight : screenWidth / 3,
      // owner
      this
    )
    this.gun.setAmmoType(ammoType)
  }

  get alive() {
    return this.health > 0
  }

  onMove(diff) {
    this.position.x += diff
    this.moveTarget.x += diff
    this.minX += diff
    this.maxX += diff

    if (this.bloodEngine)
      this.bloodEngine.emitPosition.x += diff;
  }

  onColliderNotify(collisionInfo) {
    if (collisionInfo.owner instanceof AbstractProjectile) {

      const bullet = collisionInfo.owner

      // make sure this turrent cant shoot itself
      if (bullet.owner === this.gun)
        return

      // this will stop not friendly fire
      const gun = bullet.owner
      if (gun.owner instanceof Turret ||
        gun.owner instanceof Soldier)
        return

      this.health = max(this.health - bullet.damage, 0)

      // check if this turrent is dead
      if (!this.alive) {
        gun.owner.kills += 1
        this.collider.destroy()

        this.bloodEngine = new SoldierBloodEngine(
          this.position,
          ParticleEngineManager.destroy
        )

      }

      if (collisionInfo.owner instanceof Bomb)
        return

      bullet.notifyHit()
    }

  }

  update(deltaTime) {
    this.updateGun(deltaTime)

    if (!this.alive)
      return

    this.updateCollider(deltaTime)

    // move the soldier
    const dir = Vector.sub(this.moveTarget, this.position)
    dir.normalize()
    dir.mult(this.speed * deltaTime)
    this.position.x += dir.x

    // check if we've reached our destination
    // and update the destination if we have
    const dist = Vector.dist(this.position, this.moveTarget)
    if (dist <= 0.05) {
      this.moveTarget = createVector(
        random(this.minX, this.maxX),
        this.position.y
      )
    }
  }

  updateGun(deltaTime) {
    // update any bullets in motion
    this.gun.update(
      deltaTime,
      PlayerManager.player.position
    )

    if (!this.alive)
      return

    this.gunPos = createVector(
      this.position.x + (this.width * .5),
      this.position.y - this.height
    )

    this.gun.fireWithinRange(
      this.gunPos,
      PlayerManager.player.position
    )
  }

  updateCollider() {
    const hw = this.width * .5
    const hh = this.height * .5
    this.collider.set(
      Vector.add(
        this.position,
        createVector(-hw, -hh)
      )
    )
  }

  draw() {
    const { x, y } = this.position
    if (this.alive) {
      this.drawBody(x, y)

      if (this.gun.ProjectileClass.name === "Rocket")
        this.drawRocketLauncher(x, y)

      this.drawHealth(x, y)


      this.gun.draw()
    }
  }

  drawBody(x, y) {
    const hw = this.width * .5
    fill(25, 150, 25)
    noStroke()
    rect(
      x - hw,
      y - this.height,
      hw * 2,
      this.height
    )
  }

  drawHealth(x, y) {
    const boxWidth = this.width * 3
    push()
    fill(100, 100, 100)
    noStroke()
    translate(x, y - this.height)

    // health taken in red
    stroke(255)
    fill(255, 0, 0)
    rect(
      0,
      0,
      boxWidth,
      3
    )

    // bunker health left over
    noStroke()
    fill(0, 255, 0)
    rect(
      1,
      1,
      (this.health / this.startingHealth) * boxWidth,
      2
    )
    pop()
  }

  drawRocketLauncher(x, y) {
    fill(25, 25, 25)
    const lineWidth = 6

    push()
    translate(this.gunPos.x, this.gunPos.y)
    stroke(255)
    strokeWeight(2)
    line(
      -1, 0,
      lineWidth * this.gun.forward.x, lineWidth * this.gun.forward.y
    )
    pop()
  }


}