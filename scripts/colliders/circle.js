/*
  Island Attack is a 2D game entry for Ludum Dare 38
  Copyright (C) 2017 Peter Flannery

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>
 */

class CircleCollider {

  constructor(radius, owner, notifyCb) {
    this.r = radius
    this.owner = owner
    this.notifyCallback = notifyCb
    ColliderManager.add(this)
  }

  set(position) {
    this.x = position.x
    this.y = position.y
  }

  pointInside(x, y) {
    const dx = this.x - x;
    const dy = this.y - y;
    const dist = dx * dx + dy * dy;
    const testRadius = this.r * this.r;
    return (testRadius - dist) > 0;
  }

  overlaps(collider) {
    return circlesOverlap(
      this.x,
      this.y,
      this.r,
      collider.x,
      collider.y,
      collider.r
    )
  }

  notify(collisionInfo) {
    if (this.notifyCallback)
      this.notifyCallback.call(this.owner, collisionInfo)
  }

  destroy() {
    ColliderManager.destroy(this)
  }

  debugDraw() {
    const { x, y, r } = this
    noFill()
    stroke(255, 0, 0)
    ellipse(x, y, r)
  }

}
