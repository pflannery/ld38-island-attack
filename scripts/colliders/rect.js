/*
  Island Attack is a 2D game entry for Ludum Dare 38
  Copyright (C) 2017 Peter Flannery

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>
 */

class RectCollider {

  constructor(x, y, w, h, owners, notifyCb) {
    this.x = x
    this.y = y
    this.w = w
    this.h = h
    this.owner = owners
    this.notifyCallback = notifyCb
    ColliderManager.add(this)
  }

  get r() {
    return this.x + this.w
  }

  get b() {
    return this.y + this.h
  }

  set(position) {
    this.x = position.x
    this.y = position.y
  }

  pointInside(tx, ty) {
    const { x, y, r, b } = this
    return (
      tx >= x &&
      tx < r &&
      ty >= y &&
      ty < b
    )
  }

  notify(collisionInfo) {
    if (this.notifyCallback)
      this.notifyCallback.call(this.owner, collisionInfo)
  }

  destroy() {
    ColliderManager.destroy(TouchList)
  }

  debugDraw() {
    const { x, y, w, h } = this
    noFill()
    stroke(255, 0, 0)
    rect(x, y, w, h)
  }

}