/*
  Island Attack is a 2D game entry for Ludum Dare 38
  Copyright (C) 2017 Peter Flannery

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>
 */

class ColliderManager {

  static get colliders() {
    if (!this._colliders)
      this._colliders = []

    return this._colliders
  }

  static set colliders(value) {
    this._colliders = value
  }

  static add(collider) {
    this.colliders.push(collider)
  }

  static destroy(instance) {
    let i = this.colliders.indexOf(instance)
    this.colliders.splice(i, 1)
  }

  static reset() {
    this.colliders = []
  }

  static update(deltaTime) {
    const { colliders } = this

    colliders.forEach(
      colliderA => {
        if (!colliderA.notifyCallback)
          return

        colliders.forEach(
          colliderB => {

            if (colliderA === colliderB)
              return

            if (colliderA.owner === colliderB.owner)
              return

            if (colliderA instanceof CircleCollider &&
              colliderB instanceof CircleCollider) {

              if (colliderA.overlaps(colliderB)) {
                colliderA.notify(colliderB)
              }

            } else {

              if (colliderA.pointInside(colliderB.x, colliderB.y)) {
                colliderA.notify(colliderB)
              }

            }

          }
        )

      }
    )
  }

}