/*
  Island Attack is a 2D game entry for Ludum Dare 38
  Copyright (C) 2017 Peter Flannery

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>
 */

class ExplosionManager {

  static get explosions() {
    if (!this._explosions)
      this._explosions = []

    return this._explosions
  }

  static set explosions(value) {
    this._explosions = value
  }

  static add(template, notifyCb) {
    this.explosions.push(
      new Explosion(template, notifyCb)
    )
  }

  static destroy(instance) {
    let i = ExplosionManager.explosions.indexOf(instance)
    ExplosionManager.explosions.splice(i, 1)
  }

  static reset() {
    this.explosions = []
  }

  static update(deltaTime) {
    this.explosions.forEach(
      explosion => {
        explosion.update(deltaTime)
      }
    )
  }

  static draw() {
    push()
    translate(-ChunkManager.scrollX, 0)
    this.explosions.forEach(
      explosion => {
        explosion.draw()
      }
    )
    pop()
  }

}