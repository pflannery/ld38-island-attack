/*
  Island Attack is a 2D game entry for Ludum Dare 38
  Copyright (C) 2017 Peter Flannery

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>
 */

class PlayerManager {

  static get player() {
    return this._player
  }

  static set player(value) {
    this._player = value
  }

  static reset() {
    this.player = new Player(screenCentreX, screenCentreY)
  }

  static update(deltaTime) {
    PlayerManager.player.update(deltaTime)
  }

  static draw() {
    const renderPlayer = (
      PlayerManager.player &&
      (PlayerManager.player.alive || PlayerManager.player.planeCrashSmoke)
    )
    if (!renderPlayer)
      return

    push()
    translate(-ChunkManager.scrollX, 0)
    PlayerManager.player.draw()
    pop()

    if (renderPlayer)
      // gui overlay
      this.drawStats()
  }

  static drawStats() {
    const px = 5, py = 5
    const {
      health, startingHealth,
      boostCharge, startingBoostCharge
    } = this.player
    const boxWidth = 100

    push()


    textSize(12)

    // health
    stroke(255)
    fill(255, 0, 0)
    rect(px, py, boxWidth + 1, 13)

    noStroke()
    fill(100, 200, 25)
    rect(px + 1, py + 1, (health / startingHealth) * boxWidth, 12)

    fill(255)
    noStroke()
    text("health", px + 31, py + 11)

    // boost
    stroke(255)
    fill(255, 0, 0)
    rect(px, py + 20, boxWidth + 1, 13)

    noStroke()
    fill(25, 100, 200)
    rect(px + 1, py + 21, (boostCharge / startingBoostCharge) * boxWidth, 12)

    fill(255)
    noStroke()
    text("boost", px + 31, py + 31)

    // kills
    fill(255)
    noStroke()
    text(
      "kills: " + this.player.kills + " of " + this.player.enemyCount,
      px, py + 51
    )

    pop()

  }

}