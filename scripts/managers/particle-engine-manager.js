/*
  Island Attack is a 2D game entry for Ludum Dare 38
  Copyright (C) 2017 Peter Flannery

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>
 */

class ParticleEngineManager {

  static get engines() {
    if (!this._engines)
      this._engines = []

    return this._engines
  }

  static set engines(value) {
    this._engines = value
  }

  static add(particleEngine) {
    this.engines.push(particleEngine)
  }

  static destroy(instance) {
    let i = ParticleEngineManager.engines.indexOf(instance)
    ParticleEngineManager.engines.splice(i, 1)
  }

  static reset() {
    this.engines = []
  }

  static update(deltaTime) {
    this.engines.forEach(
      engine => engine.update(deltaTime)
    )
  }

  static draw() {
    push()
    translate(-ChunkManager.scrollX, 0)
    this.engines.forEach(
      engine => engine.draw()
    )
    pop()
  }

}