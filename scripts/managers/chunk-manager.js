/*
  Island Attack is a 2D game entry for Ludum Dare 38
  Copyright (C) 2017 Peter Flannery

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>
 */

class ChunkManager {

  static get scrollX() {
    return this._scrollX
  }

  static set scrollX(value) {
    this._scrollX = value
  }

  static get viewStartX() {
    return this._viewStartX
  }

  static set viewStartX(value) {
    this._viewStartX = value
  }

  static get viewEndX() {
    return this._viewEndX
  }

  static set viewEndX(value) {
    this._viewEndX = value
  }

  static get chunkCount() {
    return 5
  }

  static get chunks() {
    if (!this._chunks)
      this._chunks = []

    return this._chunks
  }

  static set chunks(value) {
    this._chunks = value
  }

  static generate() {
    const chunkWidth = gameWorld.worldWidth / this.chunkCount
    let cx = -((this.chunkCount / 2 | 0) * chunkWidth)


    const pool = [
      SeaChunk,
      LandChunk,
      SeaChunk,
      LandChunk,
      SeaChunk
    ]

    for (let i = 0; i < this.chunkCount; i++) {
      const chunkClass = pool.pop()
      this.chunks.push(
        new chunkClass(i, cx, chunkWidth)
      )
      cx += chunkWidth
    }

  }

  static reset() {
    ChunkManager.scrollX = 0
    ChunkManager.chunks = []
  }

  static update(deltaTime) {
    const drawnChunkIndexes = []
    this.chunks.forEach(
      (chunk, index) => {
        chunk.update(deltaTime)
        if (chunk.within(this.viewStartX, this.viewEndX)) {
          drawnChunkIndexes.push(index)
        }
      }
    )

    const shouldInsertFirst = drawnChunkIndexes[0] === 0
    const shouldAppendLast =
      drawnChunkIndexes[drawnChunkIndexes.length - 1] >= this.chunks.length - 1

    // // determine direction of player
    const playerDirX = PlayerManager.player.forward.x
    if (playerDirX > 0 && shouldAppendLast) {
      // move the first chunk to last position
      const firstChunk = this.chunks.splice(0, 1)[0]
      const lastChunk = this.chunks[this.chunks.length - 1]
      firstChunk.moveStart(lastChunk.endX)
      this.chunks.push(firstChunk)
    } else if (playerDirX < 0 && shouldInsertFirst) {
      // move the last chunk to first position
      const firstChunk = this.chunks[0]
      const lastChunk = this.chunks[this.chunks.length - 1]
      this.chunks.length--
      // insert as first
      lastChunk.moveEnd(firstChunk.startX)
      this.chunks.splice(0, 0, lastChunk)
    }

    // update screen scroll based on player dir + speed
    this.scrollX += playerDirX * (PlayerManager.player.speed * deltaTime)
    this.viewStartX = this.scrollX// - screenCentreX
    this.viewEndX = this.scrollX + screenWidth
  }

  static draw(deltaTime) {
    push()
    translate(-this.scrollX, 0)
    this.chunks.forEach(
      chunk => {
        if (chunk.within(this.viewStartX, this.viewEndX)) {
          chunk.draw()
          // chunk.debugDraw()
        }
      }
    )
    pop()
  }

}