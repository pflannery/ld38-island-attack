/*
  Island Attack is a 2D game entry for Ludum Dare 38
  Copyright (C) 2017 Peter Flannery

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>
 */

class SceneManager {

  static get scenes() {
    if (!this._scenes)
      this._scenes = {
        menu: new MenuScene(),
        play: new PlayGameScene(),
        gameover: new GameOverScene(),
        win: new WinScene()
      }

    return this._scenes
  }

  static add(sceneKey, scene) {
    this._scenes[sceneKey] = scene
  }

  static get current() {
    if (!this._currentSceneKey)
      this._currentSceneKey = "menu"

    return this.scenes[this._currentSceneKey]
  }

  static loadScene(sceneKey) {
    this._currentSceneKey = sceneKey
  }

}