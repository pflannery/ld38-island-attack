/*
  Island Attack is a 2D game entry for Ludum Dare 38
  Copyright (C) 2017 Peter Flannery

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>
 */

function pointInCircle(x, y, cx, cy, cr) {
  const dx = cx - x;
  const dy = cy - y;
  const distSq = dx * dx + dy * dy;
  const testRadiusSq = cr * cr;
  return (testRadiusSq - distSq) > 0;
}

function circlesOverlap(cx1, cy1, cr1, cx2, cy2, cr2) {
  const dx = cx2 - cx1;
  const dy = cy2 - cy1;
  const distSq = dx * dx + dy * dy;
  const testRadiusSq = (cr1 * cr1) + (cr2 * cr2);

  return testRadiusSq > distSq;
}