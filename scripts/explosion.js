/*
  Island Attack is a 2D game entry for Ludum Dare 38
  Copyright (C) 2017 Peter Flannery

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>
 */

class Explosion {

  constructor(template, notifyEndCb) {
    this.particles = []
    this.particleTemplate = template
    this.notifyEndCallback = notifyEndCb
    this.hasNotified = false

    this.generateParticles()
  }

  generateParticles() {
    const template = this.particleTemplate
    for (let i = 0; i < template.count; i++) {
      this.particles.push(template.copy())
    }
  }

  update(deltaTime) {
    const particleCount = this.particles.length
    if (particleCount === 0 &&
      this.hasNotified == false &&
      this.notifyEndCallback) {
      this.notifyEndCallback(this)
      this.hasNotified = true
      return
    }

    for (let i = particleCount - 1; i >= 0; i--) {
      const particle = this.particles[i]

      if (!particle.alive) {
        this.particles.splice(i, 1)
        continue
      }

      particle.update(deltaTime)
    }

  }

  draw() {
    this.particles.forEach(
      particle => {
        particle.draw()
      }
    )
  }

}
