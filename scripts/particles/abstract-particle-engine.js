/*
  Island Attack is a 2D game entry for Ludum Dare 38
  Copyright (C) 2017 Peter Flannery

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>
 */

class AbstractParticleEngine {

  constructor(
    emitLifetime,
    emitPosition,
    rebirthRate,
    particleTemplate,
    notifyCompleteCb) {
    this.emitLifetime = emitLifetime
    this.emitElapsed = 0
    this.emitPosition = emitPosition

    this.rebirthRate = rebirthRate
    this.rebirthElasped = 0
    this.particleTemplate = particleTemplate
    this.notifyCompleteCb = notifyCompleteCb
    // this.particleMinLifeTime
    // this.particleMaxLifeTime

    this.particles = []

    ParticleEngineManager.add(this)
  }

  update(deltaTime) {
    const emitExpired = this.emitElapsed >= this.emitLifetime
    const rebirthExpired = this.rebirthElasped >= this.rebirthRate

    // test if this emitter has expired
    if (emitExpired && this.particles.length === 0) {

      if (this.notifyCompleteCb)
        this.notifyCompleteCb(this)

      this.emitElapsed = 0
      ParticleEngineManager.destroy(this)
    }
    this.emitElapsed += deltaTime

    // test if we need to spawn a new particle
    if (rebirthExpired && !emitExpired) {
      this.spawnParticle()
      this.rebirthElasped = 0
    }
    this.rebirthElasped += deltaTime

    // update the existing particles
    this.updateParticles(deltaTime)
  }

  updateParticles(deltaTime) {
    const particleCount = this.particles.length
    for (let i = particleCount - 1; i >= 0; i--) {
      const particle = this.particles[i]
      if (!particle.alive) {
        this.particles.splice(i, 1)
        continue
      }
      particle.update(deltaTime)
    }
  }

  spawnParticle() {
    this.particles.push(
      new this.particleTemplate(
        this.emitPosition
      )
    )
  }

  draw() {
    this.particles.forEach(
      particle => particle.draw()
    )
  }

}