/*
  Island Attack is a 2D game entry for Ludum Dare 38
  Copyright (C) 2017 Peter Flannery

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>
 */

class AbstractParticle {

  constructor(
    position,
    minDirection,
    maxDirection,
    lifetime,
    speed,
    size,
    count,
    colour = { r: 255, g: 255, b: 255, a: 255 }
  ) {
    this.startingLifetime = lifetime
    this.lifetime = lifetime
    this.speed = speed
    this.position = position.copy()
    this.minDirection = minDirection
    this.maxDirection = maxDirection
    this.size = size
    this.count = count
    this.colour = colour

    this.direction = createVector(
      random(minDirection.x, maxDirection.x),
      random(minDirection.y, maxDirection.y)
    )
  }

  copy() {
    return new this.constructor(
      this.position.copy(),
      this.minDirection.copy(),
      this.maxDirection.copy(),
      this.lifetime,
      this.speed,
      this.size,
      this.count
    )
  }

  get alive() {
    return this.lifetime > 0
  }

  update(deltaTime) {
    let velocity = createVector(
      this.direction.x * this.speed * deltaTime,
      this.direction.y * this.speed * deltaTime
    )

    this.position.add(velocity)
    this.lifetime -= deltaTime
  }

  draw() {
    const { x, y } = this.position
    const { r, g, b, a } = this.colour
    const hs = this.size * .5

    const fadeAlpha = (this.lifetime / this.startingLifetime) * 255

    noStroke()
    fill(r, g, b, fadeAlpha)
    ellipse(
      x - hs, y - hs,
      this.size
    )

  }
}