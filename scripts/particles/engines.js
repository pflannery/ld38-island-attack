/*
  Island Attack is a 2D game entry for Ludum Dare 38
  Copyright (C) 2017 Peter Flannery

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>
 */

class TurretSmokeEngine extends AbstractParticleEngine {

  constructor(emitPosition, notifyCompleteCb) {
    super(
      Infinity, // smoke forever
      //
      emitPosition,
      // rebirth rate
      180,
      // particle template
      TurretSmokeParticle,
      //
      notifyCompleteCb
    )
  }

}

class PlaneSmokeEngine extends AbstractParticleEngine {

  constructor(emitPosition, notifyCompleteCb) {
    super(
      Infinity, // smoke forever
      //
      emitPosition,
      // rebirth rate
      180,
      // particle template
      PlaneSmokeParticle,
      //
      notifyCompleteCb
    )
  }

}


class SoldierBloodEngine extends AbstractParticleEngine {

  constructor(emitPosition, notifyCompleteCb) {
    super(
      1600, // blood forever?
      //
      emitPosition,
      // rebirth rate
      10,
      // particle template
      SoldierExplodeParticle,
      //
      notifyCompleteCb
    )
  }

}