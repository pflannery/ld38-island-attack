/*
  Island Attack is a 2D game entry for Ludum Dare 38
  Copyright (C) 2017 Peter Flannery

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>
 */

class AirplaneExplodeParticle extends AbstractParticle {

  constructor(position) {
    super(
      position.copy(),
      // min dir
      createVector(-1, -1),
      // max dir
      createVector(1, 1),
      // particleLifetime
      800,
      // particleSpeed
      0.1,
      // particleSize
      3,
      // count
      100
    )

  }

}

class BulletExplodeParticle extends AbstractParticle {

  constructor(position) {
    super(
      position.copy(),
      // min dir
      createVector(-1, -1),
      // max dir
      createVector(1, 1),
      // particleLifetime
      100,
      // particleSpeed
      0.1,
      // particleSize
      1,
      // count
      20
    )

  }

}

class BombExplodeParticle extends AbstractParticle {

  constructor(position) {
    super(
      position.copy(),
      // min dir
      createVector(-1, -1),
      // max dir
      createVector(1, 0),
      // particleLifetime
      600,
      // particleSpeed
      0.08,
      // particleSize
      1,
      // count
      120
    )

  }

}

class TurretExplodeParticle extends AbstractParticle {

  constructor(position) {
    super(
      position.copy(),
      // min dir
      createVector(-1, -1),
      // max dir
      createVector(1, 0),
      // particleLifetime
      1000,
      // particleSpeed
      0.2,
      // particleSize
      1,
      // count
      30
    )
  }

}

class SoldierExplodeParticle extends AbstractParticle {

  constructor(position) {
    super(
      position.copy(),
      // min dir
      createVector(-0.8, -1),
      // max dir
      createVector(0.8, 0),
      // particleLifetime
      1000,
      // particleSpeed
      0.04,
      // particleSize
      2,
      // count
      0,
      { r: 255, g: 100, b: 100, a: 255 }
    )
  }

}

class PlaneSmokeParticle extends AbstractParticle {

  constructor(position, direction) {
    super(
      position.copy(),
      // min dir
      createVector(-0.7, -1),
      // max dir
      createVector(0.7, 0),
      // particleLifetime
      1000,
      // particleSpeed
      0.03,
      // particleSize
      8,
      0,
      { r: 80, g: 80, b: 65, a: 255 }
    )
  }

}

class TurretSmokeParticle extends AbstractParticle {

  constructor(position) {
    super(
      position.copy(),
      // min dir
      createVector(-0.2, -1),
      // max dir
      createVector(0.2, 0),
      // particleLifetime
      1000,
      // particleSpeed
      0.03,
      // particleSize
      5,
      0,
      { r: 80, g: 80, b: 65, a: 255 }
    )

  }

}