/*
  Island Attack is a 2D game entry for Ludum Dare 38
  Copyright (C) 2017 Peter Flannery

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>
 */

// easier namespace for p5.Vector
const Vector = p5.Vector
const gravity = 0.9
const screenWidth = 720, screenHeight = 400
const screenCentreX = screenWidth * .5
const screenCentreY = screenHeight * .5

function setup() {
  createCanvas(720, 400)
  lastFrameMs = millis() - 16

  SceneManager.loadScene("menu")
  SceneManager.scenes["play"].startGame()
  ChunkManager.update(0)
}

let lastFrameMs

function draw() {
  background(51)

  let frameMs = millis()
  let deltaTime = frameMs - lastFrameMs

  SceneManager.current.update(deltaTime)
  SceneManager.current.draw(deltaTime)

  lastFrameMs = frameMs
}
