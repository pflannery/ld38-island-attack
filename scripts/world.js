/*
  Island Attack is a 2D game entry for Ludum Dare 38
  Copyright (C) 2017 Peter Flannery

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>
 */

class World {

  constructor() {
    this.worldWidth = screenWidth * ChunkManager.chunkCount
    this.worldHeight = screenHeight
  }

  init() {
    ExplosionManager.reset()
    ParticleEngineManager.reset()
    ColliderManager.reset()
    ChunkManager.reset()
    PlayerManager.reset()
    ChunkManager.generate()
  }

  update(deltaTime) {
    PlayerManager.update(deltaTime)

    // update colliders
    ColliderManager.update(deltaTime)

    // update the chunks
    ChunkManager.update(deltaTime)

    // update any explosions
    ExplosionManager.update(deltaTime)

    // update any particle engines
    ParticleEngineManager.update(deltaTime)
  }

  draw() {
    ChunkManager.draw()

    PlayerManager.draw()

    ExplosionManager.draw()

    ParticleEngineManager.draw()
  }

}